// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_rest/shelf_rest.dart';

void main() {
  var rootRouter = router()..addAll(new AccountResource(), path: 'accounts');

  var handler = const Pipeline()
      .addMiddleware(logRequests())
      .addHandler(rootRouter.handler);

  printRoutes(rootRouter);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}

class Account {
  final String accountId;
  final String name;

  Account.build({this.accountId, this.name});

  Account.fromJson(Map json)
      : this.accountId = json['accountId'],
        this.name = json['name'];

  Map toJson() => {'accountId': accountId, 'name': name};
}

class AccountResource {
  @Get('{?name}')
  List<Account> search(String name) {
    return [new Account.build(accountId: 'A999', name: name)];
  }

  @Add('foo', const ['GET', 'PUT'])
  String foo(int blah) => 'hi';

  @Post('')
  Account create(@RequestBody() Account account) => account;

  @Put('{accountId}')
  Account update(@RequestBody() Account account) => account;

  @Get('{accountId}', middleware: logRequests)
  Account find(String accountId) => new Account.build(accountId: accountId);

  @AddAll(path: 'deposits')
  DepositResource deposits() => new DepositResource();

  void usingTheBuilder(Router r) {
    r.add('foo2', ['GET', 'PUT'], (int blah) => 'hi');
    r.addAll(new DepositResource(), path: 'deposits2');
  }
}

class Deposit {
  double amount;
  final String depositId;

  Deposit.build({this.depositId});

  Deposit.fromJson(Map json)
      : this.depositId = json['depositId'],
        this.amount = json['amount'];

  Map toJson() => {'depositId': depositId, 'amount': amount};
}

class DepositResource {
  @Put('/{depositId}')
  Deposit create(Deposit deposit) => deposit;
}
