// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_rest/shelf_rest.dart';

void main() {
  var rootRouter = router()..addAll(new AccountResource(), path: 'accounts');

  var handler = const Pipeline()
      .addMiddleware(logRequests())
      .addHandler(rootRouter.handler);

  printRoutes(rootRouter);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}

class Account extends Jsonable {
  final String accountId;
  final String name;

  Account.build({this.accountId, this.name});

  Account.fromJson(Map json)
      : this.accountId = json['accountId'],
        this.name = json['name'];

  Map toJson() => {'accountId': accountId, 'name': name};
}

class AccountResourceModel extends ResourceModel<Account> {
  Account get account => model;

  AccountResourceModel(Account account, ResourceLinks links)
      : super(account, links);

  AccountResourceModel.fromJson(Map json)
      : this(new Account.fromJson(json['account']),
            new ResourceLinks.fromJson(json['links']));

//  Map toJson() => super.toJson()..addAll({'account': account.toJson()});
}

class AccountSearchModel
    extends SearchResourceModel<Account, AccountResourceModel> {
  AccountSearchModel(
      SearchResult<AccountResourceModel> result, SearchLinks links)
      : super(result, links);
  AccountSearchModel.fromJson(Map json)
      : super.fromJson(json, (Map j) => new AccountResourceModel.fromJson(j));
}

@RestResource('accountId')
class AccountResource {
  AccountSearchModel search(
      String name, ResourceLinksFactory linksFactory, Request request,
      [int page = 1]) {
    final accounts = [new Account.build(accountId: 'A999', name: name)];

    final results = linksFactory.forCollection(
        accounts.length,
        1,
        1,
        accounts,
        (account, links) => new AccountResourceModel(account, links),
        (account) => (account.accountId));

    // TODO: should be able to automatically populate self, nextPage, previousPage
    final links =
        (new SearchLinksBuilder()..addSelf(request.requestedUri)).build();
    return new AccountSearchModel(results, links);
  }

  AccountResourceModel create(
          @RequestBody() Account account, ResourceLinksFactory linksFactory) =>
      new AccountResourceModel(account, linksFactory(account.accountId));

  AccountResourceModel update(
          @RequestBody() Account account, ResourceLinksFactory linksFactory) =>
      new AccountResourceModel(account, linksFactory(account.accountId));

  @ResourceMethod(middleware: logRequests)
  AccountResourceModel find(
          String accountId, ResourceLinksFactory linksFactory) =>
      new AccountResourceModel(
          new Account.build(accountId: accountId), linksFactory(accountId));

  @AddAll(path: 'deposits')
  DepositResource deposits() => new DepositResource();
}

class Deposit {
  double amount;
  final String depositId;

  Deposit.build({this.depositId});

  Deposit.fromJson(Map json)
      : this.depositId = json['depositId'],
        this.amount = json['amount'];

  Map toJson() => {'depositId': depositId, 'amount': amount};
}

@RestResource('depositId')
class DepositResource {
  @ResourceMethod(method: 'PUT')
  Deposit create(@RequestBody() Deposit deposit) => deposit;
}
