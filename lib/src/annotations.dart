// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.annotation;

import 'package:shelf/shelf.dart';
import 'package:shelf_bind/shelf_bind.dart';
import 'package:shelf_route/shelf_route.dart';

/// Class Annotation to indicate a REST resource with a given
/// [pathParameterName]. REST resources typically follow a common pattern for
/// the standard operations. For example a Books resource might look like
///
///     GET    /books{?author,genre}         search books
///     POST   /books                        create a new book
///     GET    /books/{bookId}               fetch a single book
///     PUT    /books/{bookId}               update a single book
///     DELETE /books/{bookId}               delete a single book
///
/// The books resource in this case would have a [pathParameterName] of `bookId`
///
/// Classes with a [RestResource] have a default naming convention for each of
/// these standard operations as follows:
///
/// * `search` to search instances of the resource
/// * `create` to create an instance of the resource
/// * `find`   to fetch an instance of the resource
/// * `update` to update an instance of the resource
/// * `delete` to delete instance of the resource
///
/// The [ResourceMethod] can be used to override these defaults. Each standard
/// operation has a corresponding instance of the [RestOperation] class
class RestResource {
  final String pathParameterName;

  const RestResource(this.pathParameterName);
}

/// A class to identify the standard [RestResource] operations
class RestOperation {
  /// the default Dart method name for a handler of this type
  final String defaultMethodName;

  /// the default HTTP method name for a handler of this type
  final String defaultHttpMethod;

  /// allows for tweaking of response headers
  final ResponseHeaders responseHeaders;

  /// whether the operation works on the collection or an individual resource.
  /// For example search operates on the collection, find on an individual
  final bool isCollectionOperation;

  const RestOperation._internal(this.defaultMethodName, this.defaultHttpMethod,
      this.isCollectionOperation,
      [this.responseHeaders = const ResponseHeaders()]);

  static const RestOperation CREATE = const RestOperation._internal(
      'create', 'POST', true, const ResponseHeaders.created());
  static const RestOperation UPDATE =
      const RestOperation._internal('update', 'PUT', false);
  static const RestOperation FIND =
      const RestOperation._internal('find', 'GET', false);
  static const RestOperation SEARCH =
      const RestOperation._internal('search', 'GET', true);
  static const RestOperation DELETE =
      const RestOperation._internal('delete', 'DELETE', false);

  static const List<RestOperation> ALL = const [
    CREATE,
    UPDATE,
    FIND,
    SEARCH,
    DELETE
  ];

  static RestOperation defaultOperationForMethodName(String methodName) {
    return ALL.firstWhere((op) => op.defaultMethodName == methodName,
        orElse: () => null);
  }
}

typedef Middleware _MiddlewareFactory();

/// An annotation on a class method that indicates it is a particular standard
/// resource [operation]. This can be used to override the default naming scheme
/// and HTTP method for operations on a [RestResource] as well as to customise
/// the operation with custom [Middleware].
class ResourceMethod {
  final String method;
  final RestOperation operation;
  final _MiddlewareFactory _middleware;

  /// [Middleware] to include on the route created for this operation
  Middleware get middleware => _middleware != null ? _middleware() : null;
  final bool validateParameters;
  final bool validateReturn;

  // TODO: should rename method to something else like overriddenMethod so can
  // hijack the name
  String get httpMethod =>
      method != null ? method : operation.defaultHttpMethod;

  const ResourceMethod(
      {this.method,
      this.operation,
      _MiddlewareFactory middleware,
      this.validateParameters,
      this.validateReturn})
      : this._middleware = middleware;

  ResourceMethod merge(ResourceMethod other) {
    return new ResourceMethod(
        method: _default(method, other.method),
        operation: _default(operation, other.operation),
        middleware: _default(_middleware, other._middleware),
        validateParameters:
            _default(validateParameters, other.validateParameters),
        validateReturn: _default(validateReturn, other.validateReturn));
  }
}

/// Base class for all annotations that mimic the methods on [Router].
/// These support all the features for the corresponding methods and are
/// therefore a complete alternative way to express the same
abstract class RouteAnnotation {
  final dynamic path;
  final _MiddlewareFactory _middleware;
  Middleware get middleware => _middleware != null ? _middleware() : null;
  final HandlerAdapter handlerAdapter;
  final PathAdapter pathAdapter;

  const RouteAnnotation(
      this.path, this._middleware, this.handlerAdapter, this.pathAdapter);
}

/// Annotation corresponding to the `add` method of [Router]
/// For example, the following
///
///     @Add('foo', const ['GET', 'PUT'])
///     String foo(int blah) => 'hi';
///
/// is equivalent to
///
///     Router r = ...;
///     r.add('foo', ['GET', 'PUT'], (int blah) => 'hi');
class Add extends RouteAnnotation {
  final List<String> methods;
  final bool exactMatch;

  const Add(path, this.methods,
      {this.exactMatch: true,
      _MiddlewareFactory middleware,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter})
      : super(path, middleware, handlerAdapter, pathAdapter);
}

/// Annotation corresponding to the `get` method of [Router]
/// For example, the following
///
///     @Get('foo')
///     String foo(int blah) => 'hi';
///
/// is equivalent to
///
///     Router r = ...;
///     r.get('foo', (int blah) => 'hi');
class Get extends Add {
  const Get(dynamic path,
      {_MiddlewareFactory middleware,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter})
      : super(path, const ['GET'],
            middleware: middleware,
            handlerAdapter: handlerAdapter,
            pathAdapter: pathAdapter);
}

/// Annotation corresponding to the `post` method of [Router]
/// For example, the following
///
///     @Post('foo')
///     String foo(int blah) => 'hi';
///
/// is equivalent to
///
///     Router r = ...;
///     r.post('foo', (int blah) => 'hi');
class Post extends Add {
  const Post(dynamic path,
      {_MiddlewareFactory middleware,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter})
      : super(path, const ['POST'],
            middleware: middleware,
            handlerAdapter: handlerAdapter,
            pathAdapter: pathAdapter);
}

/// Annotation corresponding to the `put` method of [Router]
/// For example, the following
///
///     @Put('foo')
///     String foo(int blah) => 'hi';
///
/// is equivalent to
///
///     Router r = ...;
///     r.put('foo', (int blah) => 'hi');
class Put extends Add {
  const Put(dynamic path,
      {_MiddlewareFactory middleware,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter})
      : super(path, const ['PUT'],
            middleware: middleware,
            handlerAdapter: handlerAdapter,
            pathAdapter: pathAdapter);
}

/// Annotation corresponding to the `delete` method of [Router]
/// For example, the following
///
///     @Delete('foo')
///     String foo(int blah) => 'hi';
///
/// is equivalent to
///
///     Router r = ...;
///     r.delete('foo', (int blah) => 'hi');
class Delete extends Add {
  const Delete(dynamic path,
      {_MiddlewareFactory middleware,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter})
      : super(path, const ['DELETE'],
            middleware: middleware,
            handlerAdapter: handlerAdapter,
            pathAdapter: pathAdapter);
}

/// Annotation corresponding to the `addAll` method of [Router]
/// For example, the following
///
///     @AddAll(path: 'deposits')
////    DepositResource deposits() => new DepositResource();
///
/// is equivalent to
///
///     Router r = ...;
///     r.addAll(new DepositResource(), path: 'deposits');
class AddAll extends RouteAnnotation {
  final RouteableAdapter routeableAdapter;

  const AddAll(
      {dynamic path,
      _MiddlewareFactory middleware,
      HandlerAdapter handlerAdapter,
      this.routeableAdapter,
      PathAdapter pathAdapter})
      : super(path, middleware, handlerAdapter, pathAdapter);
}

_default(value, defaultValue) => value != null ? value : defaultValue;
