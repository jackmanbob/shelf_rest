// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.handler;

import 'package:shelf/shelf.dart';
import 'package:shelf_bind/shelf_bind.dart' as b;
import 'package:shelf_rest/src/router_builder.dart';
import 'package:shelf_rest/src/router_builder_impl.dart';
import 'package:shelf_route/extend.dart' as r;

/// Creates a `shelf_rest` router builder.
///
/// This supports all the functionality of the
/// [shelf_route](https://pub.dartlang.org/packages/shelf_route) Router with the
/// following additions:
///
/// 1. The [shelf_bind](https://pub.dartlang.org/packages/shelf_bind)
/// [HandlerAdapter] is automatically installed so you can use ordinary Dart
/// functions as handlers
/// 1. The [shelf_bind](https://pub.dartlang.org/packages/shelf_bind)
/// annotations such as [RequestBody] are automatically
/// available
/// 1. You can use annotations such as [Get] and [Post] in addition to the
/// corresponding `get` and `post` methods on [Router]. These annotations are
/// just another way of configuring the same thing and you can mix and match
/// the two styles as you wish
/// 1. You can use the [RestResource] and [ResourceMethod] annotations to
/// create your routes in a higher level, consistent manner
/// 1. You can add an argument to your handler methods of type
/// [ResourceLinksFactory] to get a factory to help create
/// [HATEOAS](http://en.wikipedia.org/wiki/HATEOAS) links.
Router router(
        {path,
        String name,
        r.HandlerAdapter handlerAdapter,
        r.RouteableAdapter routeableAdapter,
        r.PathAdapter pathAdapter: r.uriTemplatePattern,
        Function fallbackHandler,
        Middleware middleware}) =>
    new ShelfRestRouterBuilder(
        fallbackHandler,
        name,
        path,
        new r.DefaultRouterBuilderAdapter(
            handlerAdapter != null ? handlerAdapter : b.handlerAdapter(),
            pathAdapter,
            routeableAdapter),
        null,
        middleware);
