# Change Log

## 0.3.4

- Strong mode changes. Changes to some generics

## 0.3.3

* Updated versions of constrain, hateoas_models, shelf_bind, shelf_path and shelf_route.

## 0.3.0

* includes [shelf_bind][shelf_bind] 0.9.0 which has many significant and some **BREAKING**
changes. Consult the change logs for [shelf_bind][shelf_bind]

## 0.2.0

* completely rewritten on top of new (rewritten) shelf_route. Should be mostly
transparent to users except for the following breaking changes:
    * shelf_rest now extends shelf_route. 
        * The previous functions such as `bindResource`, `routeableAdapter`,
         `restHandler` and `restRouter` have been removed. Now you simply 
         import `shelf_rest` *_instead of_* `shelf_route` and the handling for
         `shelf_rest` is built into the router returned via the `router()`
         function.
    * child resources are now added using the `@AddAll` annotation. The previous
    method using a map is no longer supported
* several new features including:
    * annotations for basic route methods (`@Get`, `@Post` etc)
    * helpers to create HATEOAS links
    * see README for more details

## 0.1.5-pre.*

* experimental HATEOAS support

## 0.1.4

* Support query parameters on all methods. Thanks to pajn for the contribution


## 0.1.4-pre.*

* experimental HATEOAS support

## 0.1.3

* Allow validation of parameters to be configured at the bind function and
annotation level

## 0.1.2+1

* Doco improvements

## 0.1.2

* Turned validation of parameters on by default

## 0.1.1

* Add middleware support in the `RestMethod` annotation

## 0.1.0

* First version


[mojito]: https://pub.dartlang.org/packages/mojito
[shelf]: https://pub.dartlang.org/packages/shelf
[shelf_auth]: https://pub.dartlang.org/packages/shelf_auth
[shelf_auth_session]: https://pub.dartlang.org/packages/shelf_auth_session
[shelf_route]: https://pub.dartlang.org/packages/shelf_route
[shelf_static]: https://pub.dartlang.org/packages/shelf_static
[shelf_proxy]: https://pub.dartlang.org/packages/shelf_proxy
[shelf_bind]: https://pub.dartlang.org/packages/shelf_bind
[shelf_rest]: https://pub.dartlang.org/packages/shelf_rest
[shelf_oauth]: https://pub.dartlang.org/packages/shelf_oauth
[shelf_oauth_memcache]: https://pub.dartlang.org/packages/shelf_oauth_memcache
[shelf_exception_handler]: https://pub.dartlang.org/packages/shelf_exception_handler
[backlog.io]: http://backlog.io
[routing_blog]: http://blog.backlog.io/2015/06/completely-routed.html